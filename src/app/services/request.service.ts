import { ApiConst } from '../ApiConst';
import { HttpClient, HttpResponse, HttpHeaders, HttpRequest } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { mergeMap, map } from 'rxjs/operators';
import { ApiResponse, ApiResponseSerializer } from '../models/ApiResponse';
import { Serializer } from '../serializer/Serializer';
import { isNullOrUndefined } from 'util';
import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class RequestService {
  protected apiBaseUrl = ApiConst.apiBaseUrl;
  constructor(public httpClient: HttpClient
  ) {  }

  httpOptions = {};

  get() {

    this.httpOptions = {
      headers: new HttpHeaders({
        'Authorization': 'Bearer '+ApiConst.token,
        'Content-Type': 'application/json'
      }),
      useCredentials: true
    };


    console.log(this.httpOptions)

    return this.httpClient.get('https://api.mercedes-benz.com/experimental/connectedvehicle/v1/vehicles/7E416C7D976F98DEC5')
    .subscribe( (res:any) => {
      const apiResponse = res;
    });
      
  }

  
}


