import { Injectable, Injector, Inject } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { ApiConst } from '../ApiConst';

@Injectable()
export class TokenInterceptors implements HttpInterceptor {
  constructor(private injector: Injector) { }
  intercept(
    request: HttpRequest<any>,
    next: HttpHandler
  ): Observable<HttpEvent<any>> {

    let clonedRequest;

    const headers = new HttpHeaders({
        'Authorization': 'Bearer 111f5ef8-c779-481e-bd96-b72b15774f78',
        'Content-Type': 'application/json'
      })

    
       clonedRequest = request.clone({
        headers: headers,
        url: request.url
      });
    

    
    return next.handle(clonedRequest)
      .pipe(
        map((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
            
            const response = event.body;
            if (response.StatusCode === 401) {
              
            }
            if (typeof response.Error !== 'undefined' && response.Error !== null) {
              
            }
          }
          return event;
        }),
        catchError((err: any) => {
          
          if (err instanceof HttpErrorResponse) {
            
          }
          return of(err);
        }));
  }
  
}